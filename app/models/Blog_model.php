<?php 
class Blog_model{
    private $table = 'blog';
    private $db;

    public function __construct()
    {
        $this->db = new  Database;
    }

    public function getAllBlog() {
        $this->db->query('SELECT * FROM '. $this->table);
        return $this->db->resultSet();
    }

    public function getBlogById($id) {
        $this->db->query('SELECT * FROM '. $this->table.' WHERE id=:id');
        $this->db->bind('id', $id);
        return $this->db->single();
    }

    public function tambahDataBlog($data) {
        $query = "INSERT INTO blog (penulis, judul, tulisan) VALUES (:penulis, :judul, :tulisan)";
        $this->db->query($query);
        $this->db->bind('penulis', $data['penulis']);
        $this->db->bind('judul', $data['judul']);
        $this->db->bind('tulisan', $data['tulisan']);

        $this->db->execute();
        return $this->db->rowCount();
    }

    public function hapusDataBlog($id) {
        $query = "DELETE FROM blog WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('id', $id);

        $this->db->execute();
        return $this->db->rowCount();
    }
}
?> 