<div class="card" style="width: 18rem;">
  <!-- <img class="card-img-top" src="..." alt="Card image cap"> -->
  <div class="card-body">
    <h5 class="card-title"><?= $data['blog']['penulis']; ?></h5>
    <p class="card-text"><?= $data['blog']['judul']; ?></p>
    <p class="card-text"><?= $data['blog']['tulisan']; ?></p>
    <a href="<?= BASE_URL; ?>/blog" class="btn btn-primary">Kembali</a>
  </div>
</div>