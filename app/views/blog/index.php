    <div class="row">
      <div class="col-lg-6">
        <?php Flasher::flash(); ?>
      </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <button type="button" class="btn btn-primary mb-2" data-bs-toggle="modal" data-bs-target="#formModal">
            Tambah Data Blog
            </button>
            <h3>Blog</h3>
            <ul class="list-group">
                <?php foreach($data["blog"] as $blog) :?>
                <li class="list-group-item"> 
                    <?= $blog['judul'];?>
                    <a href="<?= BASE_URL;?>/blog/hapus/<?= $blog['id'];?>" class="badge bg-danger float-end ms-1" onclick="return confirm('Yakin dek?');">Delete</a>
                    <a href="<?= BASE_URL;?>/blog/edit/<?= $blog['id'];?>" class="badge bg-warning float-end ms-1 modalEdit" data-bs-toggle="modal" data-bs-target="#formModal">Edit</a>
                    <a href="<?= BASE_URL;?>/blog/detail/<?= $blog['id'];?>" class="badge bg-primary float-end">Details</a>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="judulModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="judulModal">Tambah Data Blog</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form action="<?= BASE_URL;?>/blog/tambah" method="post">
        <div class="mb-1">
          <label for="penulis" class="form-label">Penulis</label>
          <input type="text" class="form-control" id="penulis" name="penulis">
        </div>
        <div class="mb-1">
          <label for="judul" class="form-label">Judul</label>
          <input type="text" class="form-control" id="judul" name="judul">
        </div>
        <div class="mb-1">
          <label for="tulisan" class="form-label">Tulisan</label>
          <input type="text" class="form-control" id="tulisan" name="tulisan">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Tambah Data</button>
      </form>
      </div>
    </div>
  </div>
</div>