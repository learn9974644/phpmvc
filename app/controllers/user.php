<?php 
class User extends Controller{
    public function index() {
        $data["judul"] = "User";
        $this->view("templates/header",$data);
        $this->view("user/index");
        $this->view("templates/footer");
    }
    public function profile($nama="D.N. Aidit", $pekerjaan="Ketua PKI") {
        $data["nama"] = $nama;
        $data["pekerjaan"] = $pekerjaan;
        $data["judul"] = "User/Profile";
        $this->view("templates/header",$data);
        $this->view("user/profile", $data);
        $this->view("templates/footer");
    }
}
?>